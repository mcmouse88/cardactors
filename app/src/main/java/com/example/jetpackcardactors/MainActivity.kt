package com.example.jetpackcardactors

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val actors = listOf(
            Person(
                "Arnold Schwarzenegger",
                "Actor, Bodybuilder, Governor",
                R.drawable.arnold,
                Description.arnold
            ),
            Person(
                "Sylvester Stallone",
                "Actor, Screenwriter, Director, Producer",
                R.drawable.sly,
                Description.sly
            ),
            Person("Bruce Willis", "Actor", R.drawable.bruce, Description.bruce),
            Person(
                "Mel Gibson",
                "Actor, Film director, Producer, Screenwriter",
                R.drawable.mel,
                Description.mel
            ),
            Person("Kurt Russell", "Actor", R.drawable.kurt, Description.kurt)
        )
        setContent {
            Column(modifier = Modifier
                .fillMaxSize()
                .background(Color.Black)
                .verticalScroll(rememberScrollState()))
            {
                actors.forEach { person ->
                    ListItem(person = person)
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun ListItem(person: Person) {

    val descriptionFontSize: TextUnit = MaterialTheme.typography.subtitle1.fontSize
    val descriptionFontWeight: FontWeight = FontWeight.Normal
    val descriptionTextAlign: TextAlign = TextAlign.Justify
    var expandedState by remember { mutableStateOf(false) }

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp), // Заполнить всю ширину с отступом от краев экрана (padding)
        shape = RoundedCornerShape(15.dp), // Форма с закругленными углами с параметром закругления
        elevation = 5.dp, // Эффект подъема над экраном с эффектом тени
        onClick = { // По клику изменить значение переменной Boolean
            expandedState = !expandedState
        }
    ) {
        Box {
            Column {
                Row(verticalAlignment = Alignment.CenterVertically) { // Выровнять текст по центру вертикали
                    Image(
                        painter = painterResource(id = person.getImage()), //Установить картинку по id
                        contentDescription = "image", // как я понял описание содержимого ?
                        contentScale = ContentScale.Crop, // обрезать, если картинка не умещается
                        modifier = Modifier
                            .padding(5.dp) // Сделать отступ
                            .size(64.dp) // Установить размер картинки
                            .clip(CircleShape) // придание формы картинки
                    )
                    Column(modifier = Modifier.padding(start = 15.dp)) {
                        Text(text = "Name: ${person.getName()}")
                        Text(text = "Action: ${person.getProfession()}")
                    }
                }
                Row(modifier = Modifier.padding(10.dp)) {
                    if (expandedState) {
                        Text(
                            text = person.getDescription(),
                            textAlign = descriptionTextAlign,
                            fontSize = descriptionFontSize,
                            fontWeight = descriptionFontWeight,
                            overflow = TextOverflow.Ellipsis
                        )
                    }
                }
            }
        }
    }
}