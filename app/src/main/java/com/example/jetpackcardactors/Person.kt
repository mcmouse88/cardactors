package com.example.jetpackcardactors

class Person(
    private val name: String,
    private val profession: String,
    private val image: Int,
    private val description: String
) {

    fun getName(): String {
        return name
    }

    fun getProfession(): String {
        return profession
    }

    fun getImage(): Int {
        return image
    }

    fun getDescription(): String {
        return description
    }
}