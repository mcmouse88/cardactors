package com.example.jetpackcardactors

object Description {
    const val arnold = "\t\"Arnold Alois Schwarzenegger born July 30, 1947) is an Austrian-American actor," +
            " film producer, businessman, former bodybuilder and politician who served as the 38th governor of California between 2003 and 2011." +
            " As of 2022, he is the most recent Republican governor of California. Time magazine named Schwarzenegger one of the 100 most" +
            " influential people in the world in 2004 and 2007"
    const val sly = "\t\"Sylvester Enzio Stallone born Michael Sylvester Gardenzio Stallone, July 6, 1946) is an American actor," +
            " screenwriter, director, and producer. After his beginnings as a struggling actor for a number of years upon arriving to New York City" +
            " in 1969 and later Hollywood in 1974, he won his first critical acclaim as an actor for his co-starring role" +
            " as Stanley Rosiello in The Lords of Flatbush. Stallone subsequently found gradual work as an extra or side character in films with" +
            " a sizable budget until he achieved his greatest critical and commercial success as an actor and screenwriter, starting in 1976 with his" +
            " role as boxer Rocky Balboa, in the first film of the successful Rocky series (1976–present), for which he also wrote the screenplays." +
            "In the films, Rocky is portrayed as an underdog boxer who fights numerous brutal opponents, and wins the world heavyweight championship twice."
    const val bruce = "\t\"Walter Bruce Willis (born March 19, 1955) is an American retired actor. His career began on the off-Broadway stage in the 1970s." +
            "He achieved fame with a leading role on the comedy-drama series Moonlighting (1985–1989) and appeared in over a hundred films," +
            " gaining widespread recognition as an action hero after his portrayal of John McClane in the Die Hard franchise (1988–2013) and other roles."
    const val mel = "\t\"Mel Columcille Gerard Gibson AO (born January 3, 1956) is an American actor, film director, producer, and screenwriter." +
            " He is best known for his action hero roles, particularly his breakout role as Max Rockatansky in the first three films of the post-apocalyptic action" +
            " series Mad Max and as Martin Riggs in the buddy cop film series Lethal Weapon. Born in Peekskill, New York, Gibson moved with his parents to Sydney," +
            " Australia, when he was 12 years old. He studied acting at the National Institute of Dramatic Art," +
            " where he starred opposite Judy Davis in a production of Romeo and Juliet. During the 1980s, he founded Icon Entertainment," +
            " a production company, which independent film director Atom Egoyan has called \"an alternative to the studio system\"." +
            " Director Peter Weir cast him as one of the leads in the World War I drama Gallipoli (1981), which earned Gibson a Best Actor Award" +
            " from the Australian Film Institute,[6] as well as a reputation as a serious, versatile actor."
    const val kurt = "\t\"Kurt Vogel Russell (born March 17, 1951) is an American actor. He began acting on television at the age of 12 in the western" +
            " series The Travels of Jaimie McPheeters (1963–1964). In the late 1960s, he signed a 10-year contract with The Walt Disney Company where," +
            " according to Robert Osborne, he became the studio's top star of the 1970s."
}